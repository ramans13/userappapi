const { json, response } = require('express');
const express = require('express');
const router = express.Router();
const User = require('../userSchema');


router.get('/', async (req, res) => {
    let response=[];
    try {
        const users = await User.find();
            users.forEach((user)=>{
             obj   = {
                    name : user.Firstname+user.Lastname,
                    email : user.Email
                }
                response.push(obj)
            })
        res.json(response);
    } catch (err) {
        res.send("Error " + err);
    }
});

router.post('/signup', async (req, res) => {


    const user = await User.findOne({ Email: req.body.email });

    if (user) {
        res.send({ isSuccessful: false, message: "Already Signed up" });
        return
    }

    const newUser = new User({
        Firstname: req.body.firstname,
        Lastname: req.body.lastname,
        Email: req.body.email,
        Password: req.body.password
    })

    try {
        const response = await newUser.save();
        res.send({ isSuccessful: true, message: "success" });
    } catch (err) {
        res.send("Error " + err)
    }
});

router.post('/login', async (req, res) => {

    try {
        const user = await User.findOne({ Email: req.body.email });
        if (!user) {
            res.send({ isSuccessful: false, message: "New user! Please signup." });
        }
        else if ((user.Password !== req.body.password)) {
            res.send({ isSuccessful: false, message: "Wrong Password." });
        } else {
            res.send({ isSuccessful: true, message: "success" });
        }

    } catch (err) {
        console.log("Error " + err);
    }


})











module.exports = router;
