const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const url = "mongodb://localhost/UsersApp";
const userRouter = require('./routes/userRouter');
const bodyParser = require('body-parser')



const app = express();

app.use(express.json());
app.use(cors());
app.use(bodyParser.urlencoded({extended:false}))


app.use('/user',userRouter);

mongoose.connect(url)

const con = mongoose.connection;

con.on('open',()=>{
    console.log("Server connected.......");
});

app.listen(5000,()=>{console.log("Server started....");})